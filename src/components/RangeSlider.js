import React from "react"
import { Slider, Rail, Handles, Tracks, Ticks } from "react-compound-slider"
import BarChart from "./BarChart"
import './SliderBarchart.css'

class RangeSlider extends React.Component {
    constructor(props) {
        super(props)

        const sortedData = props.data.slice().sort((a, b) => a - b)
        const roundData = []
        for (var i = 0; i < sortedData.length; i++) {
            roundData[i] = Math.floor(sortedData[i])
        }
        const range = [roundData[0], roundData[roundData.length - 1]]

        this.state = {
            domain: [0, 1000],
            update: range,
            values: range,
            inputValues: range,
            data: roundData
        }
    }

    componentDidUpdate(prevProps) {
        if (this.props.data !== prevProps.data) {
            const sortedData = prevProps.data.slice().sort((a, b) => a - b)
            const roundData = []
            for (var i = 0; i < sortedData.length; i++) {
                roundData[i] = Math.floor(sortedData[i])
            }
            const range = [roundData[0], roundData[roundData.length - 1]]
    
            this.setState({update: range, values: range, inputValues: range, data: roundData})
        }
    }

    updateSlider = (e) => {
        this.setState({ update: e, inputValues: e })
        this.props.onchangeSlider(e)
    }

    render() {
        const { domain, values, update, inputValues, data } = this.state

        return (
            <div className="container">
                <div className="row">
                    <div style={{ margin: "3%", height: "80px", width: "94%" }}>
                        <BarChart
                            data={data}
                            highlight={update}
                            domain={domain}
                        />
                        <Slider
                            mode={3}
                            step={1}
                            domain={domain}
                            rootStyle={{
                                position: "relative",
                                width: "100%"
                            }}
                            onUpdate={update =>
                                this.updateSlider(update)
                            }
                            onChange={values => this.setState({ values })}
                            values={values}
                        >
                            <Rail>
                                {({ getRailProps }) => (
                                    <div>
                                        <div className="rail-hotspot" {...getRailProps()}></div>
                                        <div className="rail"></div>
                                    </div>
                                )}
                            </Rail>
                            <Handles>
                                {({ handles, getHandleProps }) => (
                                    <div className="slider-handles">
                                        {handles.map((handle, index) => (
                                            <div role="slider"
                                                aria-valuemin={domain[0]}
                                                aria-valuemax={domain[1]}
                                                aria-valuenow={handle.value}
                                                className="handles"
                                                style={{ left: `${handle.percent}%` }}
                                                {...getHandleProps(handle.id)}
                                                key={handle.id}
                                            ><span className="handles-text">{inputValues[index]}</span></div>
                                        ))}
                                    </div>
                                )}
                            </Handles>
                            <Tracks left={false} right={false}>
                                {({ tracks, getTrackProps }) => (
                                <div className="slider-tracks">
                                    {tracks.map(({ id, source, target }) => (
                                        <div key={id}>
                                            <div className="track" style={{ left: `${source.percent}%`, width: `${target.percent -source.percent}%` }}></div>
                                            <div className="track-hotspot" style={{ left: `${source.percent}%`, width: `${target.percent -source.percent}%`}} {...getTrackProps()}></div>
                                        </div>
                                    ))}
                                </div>
                                )}
                            </Tracks>
                            <Ticks count={5}>
                                {({ ticks }) => (
                                <div className="slider-ticks">
                                    {ticks.map(tick => (
                                        <div key={tick.id}>
                                            <div className="tick" style={{ left: `${tick.percent}%` }}></div>
                                            <p
                                                className="label"
                                                style={{
                                                    marginLeft: `${-(100 / ticks.length) / 2}%`,
                                                    width: `${100 / ticks.length}%`,
                                                    left: `${tick.percent}%`
                                                }}
                                            >
                                                {tick.value}
                                            </p>
                                        </div>
                                    ))}
                                </div>
                                )}
                            </Ticks>
                        </Slider>
                    </div>
                </div>
            </div>
        )
    }
}

export default RangeSlider
import './Home.css'
import React, { useState, useEffect } from "react"
import { Link } from "react-router-dom"

import { httpHelper } from "../../helpers/httpHelper"
import RangeSlider from "../../components/RangeSlider"

const Home = () => {
    const [shoes, setShoes] = useState(null)
    const [name, setName] = useState('')
    const [categories, setCategories] = useState(null)
    const [size, setSize] = useState(null)
    const [prices, setPrices] = useState([100, 1000])
    const [sliderPrice, setSliderPrice] = useState(null)
    const [sortText, setSortText] = useState("Price")
    const [checkedStateCategory, setCheckedStateCategory] = useState(null)
    const [checkedStateSize, setCheckedStateSize] = useState(null)
    const [filteredData, setFilteredData] = useState(null)
    
    useEffect(() => {
        let categoryElem = document.getElementById('categoryDropdown')
        if(categoryElem && categoryElem.classList && categoryElem.classList.contains('show')){
            let dropdownMenuElem = document.getElementsByClassName('dropdown-category')
            dropdownMenuElem[0].classList.add('dropdown-relative')
        }
        else {
            let dropdownMenuElem = document.getElementsByClassName('dropdown-category')
            dropdownMenuElem[0].classList.remove('dropdown-relative')
        }
    }, [])

    useEffect(() => {
        const url = "http://localhost:5000/shoes"
	    const api = httpHelper()
        api
			.get(url)
			.then(res => {
				setShoes([...res])
                setFilteredData([...res])
			})
			.catch(err => console.log(err))
    }, [])
    
    useEffect(() => {
        if (shoes && shoes.length) {
            const shoesSize = shoes.map(a => a.size)
            const removeDuplicateSize = [...new Set(shoesSize)]
            const sortShoesSize = removeDuplicateSize.sort()

            const shoesCategories = shoes.map(a => a.category)
            const removeDuplicateCategory = [...new Set(shoesCategories)]

            const shoesPrice = shoes.map(a => a.price)
            const removeDuplicatePrice = [...new Set(shoesPrice)]

            setSize(sortShoesSize)
            setCategories(removeDuplicateCategory)
            setPrices(removeDuplicatePrice)

            setCheckedStateCategory(new Array(removeDuplicateCategory.length).fill(false))
            setCheckedStateSize(new Array(sortShoesSize.length).fill(false))

            const filterData = sortByKey(filteredData, sortText.toLowerCase())
            setFilteredData(filterData)
        }
    }, [shoes])

    useEffect(() => {
        if (categories && size) {
            const filterCategory = categories.filter((item, index) => checkedStateCategory[index] === true)
            const filterDataCategory = shoes.filter(item => {
                if (filterCategory && filterCategory.length) {
                    return filterCategory.includes(item.category)
                }
                return true
            })

            const filterSize = size.filter((item, index) => checkedStateSize[index] === true)
            const filterDataSize = filterDataCategory.filter(item => {
                if (filterSize && filterSize.length) {
                    return filterSize.includes(item.size)
                }
                return true
            })

            const filterDataPrice = filterDataSize.filter(item => {
                if(sliderPrice) {
                    return item.price >= sliderPrice[0] && item.price <= sliderPrice[1]
                }
                return true
            })

            const filterDataName = filterDataPrice.filter(item => {
                if (name && name != '') {
                    //let itemName = item.name.toString().toLowerCase()
                    let nameLowercase = name.toString().toLowerCase()
                    return item.name.toString().toLowerCase().includes(nameLowercase)
                }
                return true
            })

            const sortData = sortByKey(filterDataName, sortText.toLowerCase())
            
            const shoesPrice = sortData.map(a => a.price)
            const removeDuplicatePrice = [...new Set(shoesPrice)]
            if (shoesPrice && !isNaN(shoesPrice[0])) {
                setPrices(shoesPrice)
            }

            setFilteredData(sortData)
        }
    }, [sortText, checkedStateCategory, checkedStateSize, name, sliderPrice])

    const handleCategoryClick = (e) => {
        if(e.target && e.target.classList && e.target.classList.contains('show')){
            let dropdownMenuElem = document.getElementsByClassName('dropdown-category')
            dropdownMenuElem[0].classList.add('dropdown-relative')
        }
        else {
            let dropdownMenuElem = document.getElementsByClassName('dropdown-category')
            dropdownMenuElem[0].classList.remove('dropdown-relative')
        }
    }

    const handleOnChangeSearch = (e) => {
        setName(e.target.value)
    }

    const handleSortClick = (e) => {
        const valueSort = e.target.innerHTML
        setSortText(valueSort)
        const filterData = sortByKey(filteredData, valueSort.toLowerCase())
        setFilteredData(filterData)
    }

    const sortByKey = (array, key) => {
        return array.sort(function(a, b) {
            var x = a[key]; var y = b[key]
            return ((x < y) ? -1 : ((x > y) ? 1 : 0))
        })
    }

    const handleOnChangeCategory = (position) => {
        const updatedCheckedState = checkedStateCategory.map((item, index) =>
          index === position ? !item : item
        )
        setCheckedStateCategory(updatedCheckedState)
    }

    const handleOnChangeSize = (position) => {
        const updatedCheckedState = checkedStateSize.map((item, index) =>
            index === position ? !item : item
        )
        setCheckedStateSize(updatedCheckedState)
    }

    const onChangeSlider = (data) => {
        if (data && isNaN(data[0])) {
            setSliderPrice(null)
        }
        else {
            setSliderPrice(data)
        }
    }

	return (
        <div className="container-fluid">
            <div className="container border my-2 rounded" style={{backgroundColor: '#fff'}}>
                <nav className="navbar bg-body-tertiary">
                    <div className="row w-100">
                        <div className="col-4">
                            <Link className="navbar-brand fw-bold" to="/">Shoe.</Link>
                        </div>
                        <div className="col-8">
                            <div className="input-group">
                                <input className="form-control rounded-end" type="search" placeholder="Search" aria-label="Search" style={{paddingLeft: '2rem'}} onChange={(e) => handleOnChangeSearch(e)}/>
                                <span className="input-group-append position-absolute" style={{padding: '0.3rem 0.5rem'}}>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-search" viewBox="0 0 16 16">
                                        <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z"/>
                                    </svg>
                                </span>
                            </div>
                        </div>
                    </div>
                </nav>
            
                <div className="row border-top my-2">
                    <div className="col-4 border-end px-0">
                        <div className="dropdown dropdown-category-container mt-5 border-top py-2">
                            <button id="categoryDropdown" onClick={(e) => handleCategoryClick(e)} className="btn btn-transparent dropdown-toggle w-100 text-start show border-bottom border-0 fw-bold" type="button" data-bs-toggle="dropdown" aria-expanded="true">
                                Category
                            </button>
                            <div className="dropdown-menu dropdown-category border-0 show mt-3">
                                {categories && categories.map((data, index) => {
                                    return (
                                        <div className="dropdown-item position-relative" key={index}>
                                            <input type="checkbox" id={`custom-checkbox-${index}`} name={data} value={data} checked={checkedStateCategory[index]} onChange={() => handleOnChangeCategory(index)} className="form-check-input checkbox-category"/>
                                            <label htmlFor={`custom-checkbox-${index}`} className="ms-2">{data}</label>
                                            <span className="checkbox-hole" onClick={() => handleOnChangeCategory(index)}></span>
                                        </div>
                                    )}
                                )}
                            </div>
                        </div>
                        <div className="mt-3 border-top px-3 py-2">
                            <p className="fw-bold my-0 mt-2 mb-1">Price Range</p>
                            <div className="row">
                                <RangeSlider data={prices} onchangeSlider={onChangeSlider}/>
                            </div>
                        </div>
                        <div className="mt-3 px-3 py-1 border-top">
                            <p className="fw-bold my-0 mt-2">Size</p>
                            <div className="row mt-2">
                                {size && size.map((s, index) => (
                                    <div className="col-15 position-relative size-container mb-2" key={s}>
                                        <input type="checkbox" id={s} name={s} value={s} className="checkbox-size" checked={checkedStateSize[index]} onChange={() => handleOnChangeSize(index)}/>
                                        <label htmlFor={s}>{s}</label>
                                    </div>
                                ))}
                            </div>
                        </div>
                    </div>
                    <div className="col-8 content-container">
                        <div className="row my-4 mx-2">
                            <div className="col-6">
                                <h2>New Arrivals</h2>
                            </div>
                            <div className="col-6 text-end">
                                <div className="btn-group">
                                    <button type="button" className="btn btn-transparent">Sort by {sortText}</button>
                                    <button type="button" className="btn btn-transparent dropdown-toggle dropdown-toggle-split" data-bs-toggle="dropdown" aria-expanded="false">
                                        <span className="visually-hidden">Toggle Dropdown</span>
                                    </button>
                                    <ul className="dropdown-menu">
                                        <li onClick={(e) => handleSortClick(e)} style={{cursor: 'pointer'}}><span className="dropdown-item">Price</span></li>
                                        <li onClick={(e) => handleSortClick(e)} style={{cursor: 'pointer'}}><span className="dropdown-item">Size</span></li>
                                        <li onClick={(e) => handleSortClick(e)} style={{cursor: 'pointer'}}><span className="dropdown-item">Name</span></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div className="row mx-2">
                            {filteredData?.map((data) => {
                                return (
                                    <div className="card col me-2 my-2 p-0" style={{minWidth: '15rem', maxWidth: '22rem'}} key={data.id}>
                                        <div className="card-header-container px-2 pt-2">
                                            <p className="fw-light text-capitalize my-0">{data.name}</p>
                                            <p className="fw-bolder text-capitalize my-0">{data.description}</p>
                                            <div className="card-border-header-color" style={{color: data.color}}></div>
                                        </div>
                                        <div className="card-image-container px-2 pt-2">
                                            <img src={data.image} className="card-img" alt={data.image}/>
                                        </div>
                                        <div className="card-footer-container px-2 pt-2 d-flex justify-content-between">
                                            <div className="card-footer-description">
                                                <p className="fw-light my-0">Price</p>
                                                <p className="fw-bolder fs-3">$ {data.price}</p>
                                            </div>
                                            <div className="card-footer-icon-container">
                                                <img src={data.icon1} className="card-img" alt={data.icon1}/>
                                                <img src={data.icon2} className="card-img" alt={data.icon2}/>
                                            </div>
                                        </div>
                                    </div>
                                )}
                            )}
                        </div>
                    </div>
                </div>
            </div>
        </div>
	)
}

export default Home